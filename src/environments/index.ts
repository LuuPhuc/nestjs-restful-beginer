import * as dotenv from 'dotenv';
dotenv.config();

// enviroment
const NODE_ENV: string = process.env.NODE_ENV || 'local'
// port
const PORT: number = +process.env.PORT || 5000;
// mongodb
const MONGODB_LOCAL: string = process.env.LOCAL_MONGODB_URI;
// bcrypt
const SALT: number = +process.env.SALT || 10;

export {
    NODE_ENV,
    PORT,
    MONGODB_LOCAL,
    SALT
}