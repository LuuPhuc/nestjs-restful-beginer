import { hash, compare } from 'bcrypt';

import { SALT } from '../../environments';

/**
 * Hash plaintext password
 * @param password 
 * @return Password Hash
 */
export const hashPassword = async (password: string): Promise<string> => {
    return await hash(password, SALT);
};

/**
 * Compare password and PasswordHash
 * @param password 
 * @param hash 
 * @return true or false
 */
export const comparePassword = async (password: string, hash: string): Promise<boolean> => {
    return await compare(password, hash);
}