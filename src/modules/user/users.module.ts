
import { Module } from '@nestjs/common'
import { UsersService } from './users.services';
import { UsersController } from './users.controller'

@Module({
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService]
})
export class UsersModule {

}