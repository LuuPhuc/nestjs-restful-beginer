import { IsNotEmpty, IsEmail, Length } from 'class-validator';

export class CreateUserDto {

    @Length(5, 10)
    @IsNotEmpty()
    readonly name: string

    @IsEmail()
    @IsNotEmpty()
    readonly email: string

    @IsNotEmpty()
    readonly password: string
}