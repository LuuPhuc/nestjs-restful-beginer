import { IsNotEmpty } from 'class-validator';
import { UserEntity } from '../users.entity';

export class LoginResponseDto {

    @IsNotEmpty()
    readonly user: UserEntity

    @IsNotEmpty()
    readonly accessToken: string

    @IsNotEmpty()
    readonly expiresIn: number
}