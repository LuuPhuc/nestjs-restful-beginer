import {
    Injectable,
    ForbiddenException,
    NotFoundException
} from '@nestjs/common'

import { CreateUserDto } from './dto/create-user.dto';
import { getMongoRepository } from 'typeorm'
import { Validator } from 'class-validator'

import { UserEntity } from './users.entity';
import { hashPassword } from '../../utils';

export type User = any

@Injectable()
export class UsersService {

    /**
     * Register user
     * @param createUserDto 
     * @return newUser
     */
    async insert(createUserDto: CreateUserDto): Promise<User | undefined> {
        const { email } = createUserDto;

        const existedUser = await getMongoRepository(UserEntity).findOne({ email });
        if (existedUser) throw new ForbiddenException('Email already existed.');

        const newUser = await getMongoRepository(UserEntity).save(
            new UserEntity({
                ...createUserDto,
                password: await hashPassword(createUserDto.password)
            })
        )
        return newUser
    }

    /**
     * Find all user
     * @return Users
     */
    async findAll(): Promise<User[] | undefined> {
        return getMongoRepository(UserEntity).find();
    }

    /**
     * Find user by id
     * @param _id
     * @return User(Object) 
     */
    async findOne(_id: string): Promise<User | undefined> {
        const foundUser = await getMongoRepository(UserEntity).findOne({ _id })
        if (!foundUser) throw new NotFoundException('User not found.')
        return foundUser;
    }

    /**
     * Find and replace
     * @param _id 
     * @param name 
     * @param email
     * @return  User
     */
    async findOneAndReplace(_id: string, name: string, email: string): Promise<User | undefined> {
        const foundUser = await getMongoRepository(UserEntity).findOne({ _id })

        if (!foundUser) throw new NotFoundException('User not found.')

        const updateUser = await getMongoRepository(UserEntity).save(
            new UserEntity({
                ...foundUser,
                name,
                email
            })
        )
        return updateUser;
    }

    async deleteOne(_id: string): Promise<boolean | undefined> {
        const foundUser = await getMongoRepository(UserEntity).findOne({ _id })
        if (!foundUser) throw new NotFoundException('User not found.')

        return (await getMongoRepository(UserEntity).delete(foundUser))
            ? true
            : false
    }
}