import {
    Controller,
    UseGuards,
    Post,
    Get,
    Param,
    UseInterceptors,
    Put,
    Patch,
    Body,
    Delete,
    UploadedFile,
    Request
} from '@nestjs/common'

import { UserEntity } from '../user/users.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from '../user/users.services';
import { AuthService } from '../../auth/auth.service';
import { LoginResponseDto } from '../user/dto/login-response.dto';

@Controller('users')
export class UsersController {
    constructor (
        private readonly authService: AuthService,
        private readonly userService: UsersService
    ) { }

    @Get()
    findAll() {
        return this.userService.findAll();
    }

    @Post()
    async insert(@Body() createUserDto: (CreateUserDto)) {
        const newUser = await this.userService.insert(createUserDto)

        const loginResponseDto = await this.authService.login(newUser);

        return loginResponseDto;
    }

}