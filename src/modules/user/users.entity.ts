import { Entity, ObjectIdColumn, Column } from 'typeorm';

@Entity({
    name: 'users'
})

export class UserEntity {

    @ObjectIdColumn()
    _id: string

    @Column({ length: 255, nullable: false, unique: true })
    email: string

    @Column({ length: 255, nullable: false, unique: false })
    name: string

    @Column({ length: 255, nullable: false, unique: false })
    password: string

    @Column({ length: 255, nullable: false, unique: true })
    phone: string

    @Column({ nullable: true, unique: false })
    createdAt: number
    @Column({ nullable: true, unique: false })
    updatedAt: number

    constructor (partial: Partial<UserEntity>) {
        if (partial) {
            Object.assign(this, partial)
            this.createdAt = this.createdAt || +new Date()
            this.updatedAt = +new Date()
        }
    }
}