import { Entity, ObjectIdColumn, Column } from 'typeorm';

@Entity({
    name: 'products'
})

export class ProductEntity {

    @ObjectIdColumn()
    _id: string

    @Column()
    name: string

    @Column()
    title: string

    @Column()
    description: string

    @Column()
    createdAt: number

    @Column()
    updatedAt: number

    constructor (partial: Partial<ProductEntity>) {
        if (partial) {
            Object.assign(this, partial)
            this.createdAt = this.createdAt || +new Date()
            this.updatedAt = +new Date()
        }
    }
}