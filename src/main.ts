import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';


import { PORT } from './environments';

async function bootstrap() {
    try {
        const app = await NestFactory.create(AppModule);
        await app.listen(PORT!)
    } catch (e) {
        console.log(e);
    }
}
bootstrap().catch(e => {
    throw e;
})
